<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Rejected_email</fullName>
        <description>Rejected email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Activity_Rejected</template>
    </alerts>
    <alerts>
        <fullName>approval_email</fullName>
        <description>approval email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Activity_Approved</template>
    </alerts>
    <fieldUpdates>
        <fullName>Change_Status</fullName>
        <description>change status to medical editing and disclosure</description>
        <field>Status__c</field>
        <literalValue>Medical Editing and Disclosure</literalValue>
        <name>Change Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_status_to_acredited</fullName>
        <field>Status__c</field>
        <literalValue>Accredited</literalValue>
        <name>Change status to acredited</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_status_to_client_planning_draft</fullName>
        <field>Status__c</field>
        <literalValue>Client Planning/Draft</literalValue>
        <name>Change status to client planning/draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
