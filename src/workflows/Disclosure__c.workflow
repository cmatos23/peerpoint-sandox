<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Disclosure_form_Email</fullName>
        <description>Disclosure form Email</description>
        <protected>false</protected>
        <recipients>
            <field>Participant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Disclosure_Form</template>
    </alerts>
    <alerts>
        <fullName>Send_COI_Form</fullName>
        <description>Send COI Form</description>
        <protected>false</protected>
        <recipients>
            <field>COI_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/COI_Form</template>
    </alerts>
    <alerts>
        <fullName>Send_disclosure_confirmaiton</fullName>
        <description>Send disclosure confirmaiton</description>
        <protected>false</protected>
        <recipients>
            <field>Participant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Disclosure_Confirmation</template>
    </alerts>
    <fieldUpdates>
        <fullName>Participant_Account_Name</fullName>
        <field>Participant_Account_Name__c</field>
        <formula>Participant__r.Participant__r.Name</formula>
        <name>Participant Account Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Email</fullName>
        <field>Participant_Email__c</field>
        <formula>Participant__r.Participant__r.Email__c</formula>
        <name>Set Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_COI_Email</fullName>
        <field>COI_Email__c</field>
        <formula>COI_Reviewer__r.Participant__r.Email__c</formula>
        <name>Update COI Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Disclosure Send Confirmation</fullName>
        <actions>
            <name>Send_disclosure_confirmaiton</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Status__c) &amp;&amp; ISPICKVAL(Status__c , &apos;Submitted&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email from Participant to Disclosure</fullName>
        <actions>
            <name>Set_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Participant__r.Participant__r.Email__c != null</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Participant Name from Account</fullName>
        <actions>
            <name>Participant_Account_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Participant__r.Participant__r.Name != null</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Send COI</fullName>
        <actions>
            <name>Send_COI_Form</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Disclosure__c.Status__c</field>
            <operation>equals</operation>
            <value>Submitted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Disclosure__c.COI_Email__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send COI V2</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Disclosure__c.No__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_COI_Form</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Disclosure__c.Activity_Date__c</offsetFromField>
            <timeLength>60</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Send disclosure email</fullName>
        <actions>
            <name>Disclosure_form_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Disclosure__c.Status__c</field>
            <operation>equals</operation>
            <value>Sent</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update COI Email</fullName>
        <actions>
            <name>Update_COI_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT(ISBLANK( COI_Reviewer__c )),
ISBLANK(COI_Email__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
