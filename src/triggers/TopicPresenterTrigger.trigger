trigger TopicPresenterTrigger on Topic_Presenter__c (after insert, after update) {
    if(Trigger.isAfter && Trigger.isUpdate){
        TopicPresenterTriggerService.handleAfterUpdate(Trigger.newMap, Trigger.oldMap);
    }
    if(Trigger.isAfter && Trigger.isInsert){
        //System.debug('trigger active');
        TopicPresenterTriggerService.handleAfterInsert(Trigger.newMap);

    }
}