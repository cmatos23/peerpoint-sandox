public class SiteDisclosureFormController {

    public Disclosure__c disclosure {get; set;}
    public Participant__c participant {get; set;}
    public Agenda__c agenda {get; set;}
    public List<DiscRelWrapper> discRelWrappers {get; set;}
    public Integer idToRemove {get; set;}

    // Participation Levels
    public boolean presenterCheck {get; set;}
    public boolean reviewerCheck {get; set;}
    public boolean otherCheck {get; set;}

    public SiteDisclosureFormController() {
    }

    public PageReference checkIfSubmitted() {
        if (ApexPages.currentPage().getParameters().get('did') == null && ApexPages.currentPage().getParameters().get('submit') == null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'In order to fill in this form, you need to be a participant!'));
            return null;
        }
        if (ApexPages.currentPage().getParameters().get('submit') != null) {
            return null;
        }
        System.debug(ApexPages.currentPage().getParameters().get('did'));
        disclosure = [Select id, Agenda__c, Discussing_off_label_use_of__c, In_compliance_with_HIPAA__c, Level_of_participation_in_the_activity__c, No__c,
                Participant__r.Name, Participant_Account_Name__c, Participant_Email__c, Status__c, (Select id, Commercial_Interest__c, Disclosure__c, For_what_Role__c,
                What_was_received__c from Disclosure_Relationships__r) from Disclosure__c where id = :ApexPages.currentPage().getParameters().get('did') limit 1];
        if (disclosure.Status__c == 'Submitted' || disclosure.Status__c == 'Negative Disclosure' || disclosure.Status__c == 'Positive Disclosure') {
            PageReference thisPage = Page.SiteDisclosureForm;
            thisPage.getParameters().put('submit', 'true');
            thisPage.setRedirect(true);
            return thisPage;
        }
        if (disclosure.Level_of_participation_in_the_activity__c != null) {
            presenterCheck = disclosure.Level_of_participation_in_the_activity__c.indexOf('Presenter') != -1;
            reviewerCheck = disclosure.Level_of_participation_in_the_activity__c.indexOf('Medical Editor/Reviewer') != -1;
            otherCheck = disclosure.Level_of_participation_in_the_activity__c.indexOf('Other (Planning committee, Management, etc)') != -1;
        }
        participant = [Select id, Agenda__c, Participant__c, Participant__r.Name, Type__c, Name from Participant__c where id = :disclosure.Participant__c limit 1];
        if (participant.Agenda__c != null) {
            agenda = [select id, Name from Agenda__c where id = :participant.Agenda__c limit 1];
            disclosure.Agenda__c = agenda.Id;
        }
        disclosure.Participant__c = participant.Id;
        discRelWrappers = new List<DiscRelWrapper>();
        for (Integer i = 0; i < disclosure.Disclosure_Relationships__r.size(); i++) {
            discRelWrappers.add(new DiscRelWrapper(disclosure.Disclosure_Relationships__r[i], i));
        }
        if (discRelWrappers.size() == 0) {
            discRelWrappers.add(new DiscRelWrapper(new Disclosure_Relationship__c(), 0));
        }
        return null;
    }

    public PageReference removeRel() {
        for (Integer i = 0; i < discRelWrappers.size(); i++) {
            if (discRelWrappers.get(i).identifier == idToRemove) {
                DiscRelWrapper wrapper = discRelWrappers.remove(i);
                if (wrapper.discRel.Id != null) {
                    delete wrapper.discRel;
                }
                break;
            }
        }
        return null;
    }

    public PageReference addRel() {
        if (discRelWrappers.size() == 0) {
            discRelWrappers.add(new DiscRelWrapper(new Disclosure_Relationship__c(), 0));
        } else {
            Integer previousId = discRelWrappers.get(discRelWrappers.size() - 1).identifier;
            discRelWrappers.add(new DiscRelWrapper(new Disclosure_Relationship__c(), previousId + 1));
        }
        return null;
    }

    public PageReference submit() {
        if (disclosure.Acknowledgement__c == false || disclosure.Full_Legal_Name__c == null || disclosure.Sign_Date__c == null) {
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, 'Need to fill in all required fields!'));
            return null;
        }
        if (disclosure.No__c) {
            disclosure.Status__c = 'Negative Disclosure';
        } else {
            disclosure.Status__c = 'Positive Disclosure';
        }
        save();

        PageReference thisPage = Page.SiteDisclosureForm;
        thisPage.getParameters().put('submit', 'true');
        return thisPage;
    }

    public class DiscRelWrapper {
        public Integer identifier {get; set;}
        public Disclosure_Relationship__c discRel {get; set;}

        public DiscRelWrapper(Disclosure_Relationship__c discRel, Integer identifier) {
            this.discRel = discRel;
            this.identifier = identifier;
        }
    }

    public PageReference save() {
        List<String> participationLevels = new List<String>();
        if (presenterCheck != null && presenterCheck) { participationLevels.add('Presenter');}
        if (reviewerCheck != null && reviewerCheck) { participationLevels.add('Medical Editor/Reviewer');}
        if (otherCheck != null && otherCheck) { participationLevels.add('Other (Planning committee, Management, etc)');}
        disclosure.Level_of_participation_in_the_activity__c = String.join(participationLevels, ';');
        update disclosure;

        if (disclosure.No__c != true) {
            List<Disclosure_Relationship__c> discRelsToUpdate = new List<Disclosure_Relationship__c>();
            for (DiscRelWrapper wrapper : discRelWrappers) {
                if (wrapper.discRel.Disclosure__c == null) {
                    wrapper.discRel.Disclosure__c = disclosure.Id;
                }
                discRelsToUpdate.add(wrapper.discRel);
            }
            upsert discRelsToUpdate;
        } else {
            List<Disclosure_Relationship__c> discRelsToDelete = new List<Disclosure_Relationship__c>();
            for (DiscRelWrapper wrapper : discRelWrappers) {
                if (wrapper.discRel.Id != null) {
                    discRelsToDelete.add(wrapper.discRel);
                }
            }
            delete discRelsToDelete;
            discRelWrappers.clear();
            discRelWrappers.add(new DiscRelWrapper(new Disclosure_Relationship__c(), 0));
        }
        return null;
    }
}