/**
 * Created by ChrisMatos on 9/2/2016.
 */

@isTest
public with sharing class ParticipantTriggerTest {
    @TestSetup
    public static void setup(){
        Account a = new Account();
        a.Name = 'Test Account';

        insert a;

        Agenda__c agenda = new Agenda__c();
        agenda.Name = 'Test Agenda';
        agenda.Account__c = a.id;

        insert agenda;

        Topic__c topic = new Topic__c();
        topic.Name = 'Test Topic';
        topic.Agenda__c = agenda.id;

        insert topic;
    }

    public static testMethod void itShouldGenerateDisclosureIfNewParticipant(){
        
        Topic__c topic = [SELECT id, Name, Agenda__c FROM Topic__c WHERE Name = 'Test Topic' LIMIT 1];
        
		Participant__c participant = new Participant__c();
        participant.Topic__c = topic.id;
        participant.Agenda__c = topic.Agenda__c;
        participant.Type__c = UtilConstants.NonPresenter;
        insert participant;
        
        List<Disclosure__c> disclosures = [SELECT id, Name FROM Disclosure__c];
        System.assertEquals(0, disclosures.size());
        
        participant.Type__c = UtilConstants.Presenter;
        update participant;
        
        disclosures = [SELECT id, Name FROM Disclosure__c];
        System.assertEquals(1, disclosures.size());
    }
    public static testMethod void itShouldNotGenerateDisclosureIfParticipantEmailExistsOnActivity(){
 		Agenda__c agenda = [SELECT id, Name FROM Agenda__c WHERE Name = 'Test Agenda' LIMIT 1];
        
        Topic__c topic = [SELECT id, Name, Agenda__c FROM Topic__c WHERE Name = 'Test Topic' LIMIT 1];
        
        Topic__c topic2 = new Topic__c();
        topic2.Name = 'Test Topic2';
        topic2.Agenda__c = agenda.id;

        insert topic2;
        
        
        Participant__c participant = new Participant__c();
        participant.Topic__c = topic.id;
        participant.Agenda__c = topic.Agenda__c;
        participant.Type__c = UtilConstants.Presenter;
        participant.Email__c = 'test@test.com';
        participant.Name = 'John Test';
        insert participant;
               
		Participant__c participant2 = new Participant__c();
        participant2.Topic__c = topic2.id;
        participant2.Agenda__c = topic2.Agenda__c;
        participant2.Type__c = UtilConstants.Presenter;
        participant2.Email__c = 'test@test.com';
        participant2.Name = 'John Test';
        insert participant2;
       
        
        List<Disclosure__c> disclosures = [SELECT id, Name FROM Disclosure__c WHERE Participant__r.Email__c = :participant.Email__c];
        System.assertEquals(1, disclosures.size());
    }
}