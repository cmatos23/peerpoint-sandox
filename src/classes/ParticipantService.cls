/**
 * Created by ChrisMatos on 8/26/2016.
 */

public with sharing class ParticipantService {
    public static void handleAfterUpdate(Map<Id, Participant__c> newMap, Map<Id, Participant__c> oldMap){
        Map<Id, Participant__c> participantsByIds = new Map<id, Participant__c>();
        for(Id key : newMap.keySet()) {
            if (newMap.get(key).Type__c != oldMap.get(key).Type__c && newMap.get(key).Type__c.contains(UtilConstants.Presenter)) {
                participantsByIds.put(key, newMap.get(key));
            }
        }
        generateDisclosures(participantsByIds);
    }
    public static void handleAfterInsert(Map<Id, Participant__c> newMap) {
        Map<Id, Participant__c> participantsByIds = new Map<id, Participant__c>();
        for(Id key : newMap.keySet()) {
            if (newMap.get(key).Type__c != null && newMap.get(key).Type__c.contains(UtilConstants.Presenter)) {
                participantsByIds.put(key, newMap.get(key));
            }
        }
        generateDisclosures(participantsByIds);
    }
    public static void generateDisclosures(Map<Id, Participant__c> newMap){
       // List<Id> participantIds = new List<Id>();
        List<String> emailAddresses = new List<String>();
        List<Id> agendaIds = new List<Id>();
        for(Participant__c p : newMap.values()){
            agendaIds.add(p.Agenda__c);
            emailAddresses.add(p.Email__c);
            //participantIds.add(p.id);
        }
        List<Disclosure__c> disclosures = [SELECT id, Agenda__c, Participant__r.Email__c, Participant__c FROM Disclosure__c WHERE Participant__r.Email__c IN :emailAddresses AND Agenda__c IN :agendaIds];

        System.debug(disclosures);
        Map<Id, Map<String, Disclosure__c>> disclosuresByEmailByAgendaIds = new Map<Id, Map<String, Disclosure__c>>();

        for(Id aId : agendaIds){
            if(!disclosuresByEmailByAgendaIds.containsKey(aId)){
                disclosuresByEmailByAgendaIds.put(aId, new Map<String, Disclosure__c>());
            }
        }
        for(Disclosure__c disclosure : disclosures){
            if(disclosuresByEmailByAgendaIds.containsKey(disclosure.Agenda__c)){
                disclosuresByEmailByAgendaIds.get(disclosure.Agenda__c).put(disclosure.Participant__r.Email__c, disclosure);
            }

        }

        System.debug(disclosuresByEmailByAgendaIds);
        List<Disclosure__c> disclosuresToInsert = new List<Disclosure__c>();

        for(Participant__c participant : newMap.values()){
            if(disclosuresByEmailByAgendaIds.get(participant.Agenda__c).get(participant.Email__c) == null){
                Disclosure__c disclosure = new Disclosure__c();
                disclosure.Agenda__c = participant.Agenda__c;
                disclosure.Level_of_participation_in_the_activity__c = participant.Type__c;
                disclosure.Participant__c = participant.id;
                disclosure.Status__c = 'Sent';

                disclosuresToInsert.add(disclosure);

            }


        }
        System.debug(disclosuresToInsert);
        try{
            insert disclosuresToInsert;
        }
        catch(Exception e){
            System.debug(e.getStackTraceString());
            System.debug(e.getMessage());
        }



    }
}