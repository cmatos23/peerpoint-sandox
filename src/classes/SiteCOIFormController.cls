public class SiteCOIFormController {

    public COI__c coi {get; set;}
    public Disclosure__c disclosure {get; set;}
    public Boolean approvedCheck {get; set;}
    public Boolean declinedCheck {get; set;}

    public SiteCOIFormController() {
        if (ApexPages.currentPage().getParameters().get('id') == null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'In order to fill in this form, it needs to have a disclosure!'));
            return;
        }
        disclosure = [Select id, Participant__r.Name from Disclosure__c where id = :ApexPages.currentPage().getParameters().get('id')];
        coi = new COI__c();
        coi.Disclosure__c = disclosure.Id;
        approvedCheck = false;
        declinedCheck = false;
    }

    public PageReference save() {
        if (approvedCheck) {
            coi.CME_Content_approved_or_denied__c = 'Approved';
        } else if (declinedCheck){
            coi.CME_Content_approved_or_denied__c = 'Denied';
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You need to either approve or decline the form!'));
            return null;
        }
        coi.Status__c = 'Created';
        insert coi;

        PageReference thisPage = Page.SiteCOIForm;
        thisPage.getParameters().put('submitted', 'true');
        return thisPage;
    }
}