/**
 * Created by ChrisMatos on 6/17/2016.
 */
@isTest
public with sharing class TestUploaderExtension {

    static testMethod void itShouldProcessTheCSV(){
        Account a = new Account(FirstName='TestAccount', LastName = 'LastName');
        a.RecordTypeId = [SELECT Id from RecordType where Name='Person Account'].id;
        insert a;
        Agenda__c ag = new Agenda__c(Name = 'TestAgenda', Account__c = a.id);
        insert ag;
        Topic__c t = new Topic__c(Agenda__c=ag.id, Name='Test Topic1');
        insert t;
        Participant__c p = new Participant__c(Agenda__c = ag.id, ExternalID__c = 'g2oo@google.com', Topic__c = t.id);
        insert p;
        UploaderExtension controller = new UploaderExtension(new ApexPages.StandardController(ag));
        controller.Next();
        controller.Back();
        System.assertEquals(controller.currentStep, 1);
        controller.agenda = ag;
        controller.fName = 'test.csv';
        controller.csvFile =  Blob.valueOf('topic_name,topic_Activity_Version_Course_Code_or_ID__c,topic_Offering_Self_Assessment_Cre__c,topic_Projected_Self_Assessment_Credit_Hours__c,Presenter First Name 1,Presenter Last Name 1,Presenter Email 1,Presenter First Name 2,Presenter Last Name 2,Presenter Email 2,Presenter First Name 3,Presenter Last Name 3,Presenter Email 3,Presenter First Name 4,Presenter Last Name 4,Presenter Email 4,Reviewer First Name,Reviewer Last Name,Reviewer Email\nTest Topic1,11,,13,John,Bill,csm@google.com,Bill,Bob,cs@blah.com,Chris,Joe,goo@google.com,Lilly,LillyBob,ll@bla.com,Lon Lon,Ranch,llr@google.com\nTest Topic2,2,2,3,Bill,John,csm2@google.com,Bill2,Bob2,c2s@blah.com,Chris2,Joe2,g2oo@google.com,Bob,BobLilly,bl@bla.com,Lon Lon,Ranch The Sequel,llr2@google.com');
        controller.uploadCSV();
        System.debug(controller.uRows.size());
        System.assert(controller.uRows.size() == 2);

    }
    static testMethod void itShouldCreateRecords(){
        Account a = new Account(FirstName='TestAccount', LastName = 'LastName');
        a.RecordTypeId = [SELECT Id from RecordType where Name='Person Account'].id;
        insert a;
        Agenda__c ag = new Agenda__c(Name = 'TestAgenda', Account__c = a.id);
        insert ag;
        UploaderExtension controller = new UploaderExtension(new ApexPages.StandardController(ag));
        controller.agenda = ag;      
        controller.fName = 'test.xlsx';
        controller.csvFile =  Blob.valueOf('topic_name,topic_Activity_Version_Course_Code_or_ID__c,topic_Offering_Self_Assessment_Cre__c,topic_Projected_Self_Assessment_Credit_Hours__c,Presenter First Name 1,Presenter Last Name 1,Presenter Email 1,Presenter First Name 2,Presenter Last Name 2,Presenter Email 2,Presenter First Name 3,Presenter Last Name 3,Presenter Email 3,Presenter First Name 4,Presenter Last Name 4,Presenter Email 4,Reviewer First Name,Reviewer Last Name,Reviewer Email\nTest Topic1,11,,13,John,Bill,csm@google.com,Bill,Bob,cs@blah.com,Chris,Joe,goo@google.com,Lilly,LillyBob,ll@bla.com,Lon Lon,Ranch,llr@google.com\nTest Topic2,2,2,3,Bill,Bob,cs@blah.com,Bill2,Bob2,c2s@blah.com,Chris2,Joe2,g2oo@google.com,Bob,BobLilly,bl@bla.com,Lon Lon,Ranch The Sequel,llr2@google.com');
        controller.uploadCSV();
        
        List<ApexPages.Message> msgs = ApexPages.getMessages();
        System.assert(msgs.size() == 2);
        //one message is agenda id missing, and second shoud be invalid file format
        System.assertEquals('Invalid file format. Must be a \'.csv\' file', msgs.get(1).getDetail());
        
        controller.fName = 'test.csv';
        controller.uploadCSV();
        controller.commitData();
        Topic__c top1 = [SELECT Name from Topic__c WHERE Name = 'Test Topic1'];
        System.assert(top1 !=null);
        Topic__c top2 = [SELECT Name from Topic__c WHERE Name = 'Test Topic2'];
        System.assert(top2 !=null);

        Participant__c[] parts = [SELECT id, ExternalID__c from Participant__c];
        //Topic_Role__c[] trs = [SELECT Name from Topic_Role__c];
        //System.assert(trs.size()==10);
        System.assert(parts.size() == 10);
        //duplicate account in test data, should have one less than parts and roles


    }
}