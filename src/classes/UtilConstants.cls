/**
 * Created by ChrisMatos on 10/6/2016.
 */

global class UtilConstants {
    global static final String Presenter = 'Presenter';
    global static final String PresenterUnavailable = 'Presenter Name Not Available';
    global static final String NonPresenter = 'Non-presenter: Planning/Chair Medical Reviewer';
    global static final String PositiveDisclosure = 'Positive Disclosure';
}