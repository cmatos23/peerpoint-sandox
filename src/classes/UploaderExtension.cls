/**
 * Created by ChrisMatos on 6/15/2016.
 */

public without sharing class UploaderExtension {
    public Integer currentStep{get;set;} //determines which panel is currently rendered
    public ApexPages.StandardController myController{get;set;}
    public Blob csvFile{get;set;} //blob to be converted
    public String fileContent{get;set;}
    public String fName{get;set;}
    public String[] fileLines {get;set;}
    public Agenda__c agenda{get;set;}

    public UploadRow[] uRows{get;set;}//wrapper classes
    public Map<Integer, String> colHeaders;
    
    public Map<Integer, String> colHead{get;set;}
    public List<RowWrapper> colRows{get;set;}
    public Map<Integer, Boolean> requiredCols{get;set;}//map of which columns are required
    public Boolean isMissingRequired{get;set;}//for button rendering
    //Topic__c[] displayTopics{get;set;}

    public Map<String, Topic__c> topicsByExtId;
  //  public Map<String, Account> personsByExtId;
    public Map<String, Participant__c> participantsByExtId;

    public class RowWrapper{
        public List<CellWrapper> columns{get;set;}
        public String topicExtId;
    }

    public class CellWrapper{
        public Boolean isChanged{get;set;}
        public Boolean isNew{get;set;}
        public Boolean isRequired{get; set;}
        public String data{get;set;}
        public Integer colNum{get; set;}
        public String accountExtId{get;set;}
        public String participantExtId{get;set;}
        //public String topicRoleExtId{get;set;}

    }
    public UploaderExtension(ApexPages.StandardController controller){
        this.myController = controller;
        this.fileLines = new String[]{};
        //cd = new ChangedData();
        //nd = new NewData();
        colHeaders = new Map<Integer, String>();
        colHead = new Map<Integer, String>();
        colRows = new List<RowWrapper>();

        requiredCols = new Map<Integer, Boolean>();
        topicsByExtId = new Map<String, Topic__c>();
        //personsByExtId= new Map<String, Account>();
        participantsByExtId= new Map<String, Participant__c>();
       // topicRolesByExtId= new Map<String, Topic_Role__c>();


        isMissingRequired = false;
        currentStep = 1;

        // query for our opportunity
        String agendaId = ApexPages.currentPage().getParameters().get('id');

        uRows = new UploadRow[]{};
        if(agendaId != null) {
            this.agenda = this.getAgenda(this.mycontroller.getId());
        }
        else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No Agenda ID Parameter'));
            ApexPages.currentPage().getParameters().put('error', 'noparam');
        }
    }

    public Agenda__c getAgenda(String Id){
        return [SELECT Id, Name FROM Agenda__c WHERE Id = :Id LIMIT 1];
    }
    /*Upload the CSV file entered by the user*/
    public PageReference uploadCSV(){
        isMissingRequired = false;
        Map<String, SObjectField> topicFields = Schema.getGlobalDescribe().get('Topic__c').getDescribe().fields.getMap();
        colHead = new Map<Integer, String>();
        colRows = new List<RowWrapper>();
        List<User> ownerOfPart = [SELECT Id from User where UserName ='kevincallahan@peerpt.com' LIMIT 1];
        //System.debug('uploadCSV called');
        try{
            if(csvFile == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No file selected. Please select \'Choose File\' and upload a file'));
                ApexPages.currentPage().getParameters().put('error', 'nofile');
                return null;
            }
            else if(fName.indexOf('.csv') == -1){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid file format. Must be a \'.csv\' file'));
                ApexPages.currentPage().getParameters().put('error', 'nofileformat');
                return null;
            }
            fileContent = blobToString(csvFile, 'ISO-8859-1');

            //seperate rows of excel sheet
            fileLines = fileContent.split('\n');
            //System.debug(fileLines);
            String[] columns = new String[]{};
            System.debug('file lines size = ' + fileLines.size());

            for(Integer r = 0; r < fileLines.size(); r++){
                columns = fileLines[r].split(',');
                if(r == 0){
                    //column headers
                    for(Integer c=0; c < columns.size(); c++){
                        colHeaders.put(c, columns[c]);
                        colHead.put(c, columns[c]);

                        requiredCols.put(c, false);
                        if(colHeaders.get(c).startsWith('topic_')) {
                            String fieldName = colHeaders.get(c).substringAfter('topic_');

                            //System.debug('topic field: ' + topicFields.get(fieldName));
                            //check if field name given exists
                            if (topicFields.get(fieldName) == null) {
                                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid Topic Field \'' + fieldName + '\' does not exist'));
                                ApexPages.currentPage().getParameters().put('error', 'nofield');
                                return null;
                            }
                        }
                    }

                }
                else{
                    //actual data if down here

                    RowWrapper rw = new RowWrapper();
                    rw.columns = new List<CellWrapper>();

                    UploadRow uRow = new UploadRow();
                    uRow.theTopic = new Topic__c();
                    //uRow.personAccounts = new Map<String,Account>();
                    uRow.participants = new Map<String,Participant__c>();
                    //uRow.topicRoles = new Map<String,Topic_Role__c>();

                    uRow.theTopic.put('Agenda__c', agenda.id);
                    //System.debug(columns);
                    //loop through field values
                    for(Integer c= 0; c< columns.size(); c++){
                        String colVal = '*';
                        if(String.isNotBlank(columns.get(c))){
                            colVal = columns.get(c);
                        }

                        CellWrapper cw = new CellWrapper();

                        cw.colNum = c;
                        cw.data = colVal;


                        String columnHeader = colHeaders.get(c);
                        //System.debug(columnHeader);
                        if(columnHeader.startsWith('topic_')){
                            //topic field
                            String fieldName = columnHeader.substringAfter('topic_');
                            //check if field name given exists

                            String titleName;
                            if (fieldName == 'name') {
                                cw.isRequired = true;
                                requiredCols.put(c, true);
                                if(columns[c] != null) {
                                    if(columns[c] != '') {

                                        titleName = columns[c];
                                    }
                                    else {
                                        isMissingRequired = true;//for button rendering
                                    }
                                }
                            }

                            if(columns[c] != '') {

                                if (topicFields.get(fieldName).getDescribe().getType() == DisplayType.DOUBLE) {
                                    uRow.theTopic.put(fieldName, Double.valueOf(columns[c]));

                                }
                                else {
                                    uRow.theTopic.put(fieldName, columns[c]);
                                }
                            }

                            colHead.put(c, topicFields.get(fieldName).getDescribe().getLabel());

                            rw.columns.add(cw);
                            //displayTopics.add(uRow.theTopic);
                            // System.debug(uRow.topic);
                        }
                        else if(columnHeader.startsWith('Presenter') || columnHeader.startsWith('Reviewer')){
                            //participant
                            //this assumes order will be first name, then last name, then email


                            //System.debug('requiredCols: ' + requiredCols.get(12));
                            //if(columns[c+2] == null || columns[c+2] == ''){
                               // isMissingRequired=true;
                           // }

                            Boolean done = false;
                            String firstNameCol, lastNameCol, emailExtIdCol, salutationCol;
                            Integer currIndex = c;
                            Integer emailIndex = -1;
                            Integer firstIndex = -1;
                            Integer lastIndex = -1;
                            Integer salutationIndex = -1;
                            //gather info about participant and account

                            while(done == false){

                                if(colHeaders.get(currIndex).indexOf('First Name') != -1){
                                    firstNameCol = columns[currIndex].trim();
                                    firstIndex = currIndex;
                                }
                                else if(colHeaders.get(currIndex).indexOf('Last Name') != -1){
                                    lastNameCol = columns[currIndex].trim();
                                    lastIndex = currIndex;
                                }
                                else if(colHeaders.get(currIndex).indexOf('Email') != -1) {
                                    emailExtIdCol = columns[currIndex].trim();
                                    requiredCols.put(currIndex, true);
                                    emailIndex = currIndex;
                                    done = true;
                                    //break;//done
                                }
                                else if(colHeaders.get(currIndex).indexOf('Salutation') != -1){
                                    salutationCol = columns[currIndex].trim();
                                    salutationIndex = currIndex;
                                }
                                else{
                                    //shouldn't get here
                                    break;
                                }

                                currIndex++;
                            }
                            //columns[c] should be email at this point
                            //owner needs to be set as admin
                            Participant__c p = new Participant__c();
                            p.Email__c = emailExtIdCol;
                            p.Agenda__c = agenda.id;
                            if(firstNameCol == '*'){
                                p.Name = lastNameCol;
                            }
                            else{
                                p.Name = firstNameCol + ' ' + lastNameCol;
                            }
                            p.Salutation__c = salutationCol;

                            Account a = new Account();
                            a.ExtID__c = emailExtIdCol;
                            a.Salutation = salutationCol;
                            a.FirstName = firstNameCol;
                            a.LastName = lastNameCol;
                            a.Email__c = emailExtIdCol;

                            if(ownerOfPart.size() > 0){
                                p.OwnerId = ownerOfPart.get(0).id;
                                a.OwnerId = ownerOfPart.get(0).id;
                            }

                            Topic_Role__c tr = new Topic_Role__c(ExternalID__c = agenda.Name + uRow.theTopic.Name + emailExtIdCol);
                            if(columnHeader.startsWith('Reviewer')){
                                tr.ExternalID__c = tr.ExternalID__c + 'Reviewer';
                                tr.Role__c = 'Reviewer';
                                p.Type__c = UtilConstants.NonPresenter;
                                p.ExternalID__c = agenda.Name + UtilConstants.NonPresenter + emailExtIdCol;
                                //System.debug('ext id: ' + tr.ExternalID__c);
                            }
                            else{//speaker
                              //  tr.ExternalID__c = tr.ExternalID__c + 'Speaker';
                             //   tr.Role__c = 'Speaker';
                                p.Type__c = UtilConstants.Presenter;
                                p.ExternalID__c = agenda.Name + uRow.theTopic.Name + emailExtIdCol;
                            }
                            //System.debug(String.isNotBlank(columns[c+2]));
                            if(emailIndex >= 0 && String.isNotBlank(columns[emailIndex])){//if email is null, do not upload
                                //uRow.personAccounts.put(emailExtIdCol,a);
                                uRow.participants.put(emailExtIdCol,p);
                               // uRow.topicRoles.put(emailExtIdCol,tr);
                            }

                            for(Integer i = c; i <  currIndex; i++){
                                CellWrapper cell= new CellWrapper();
                                cell.colNum = i;
                                String valData = '*';
                                if(String.isNotBlank(columns.get(i))){
                                    valData = columns.get(i);
                                }
                                cell.data = valData;
                                cell.accountExtId = a.ExtID__c;
                                cell.participantExtId = p.ExternalID__c;
                               // cell.topicRoleExtId = tr.ExternalID__c;

                                if(i == emailIndex){
                                    //if(firstIndex < 0){
                                        //depends on email coming after both first and last index
                                    //System.debug('firstIndex: ' + firstIndex +' lastIndex: '+lastIndex );
                                        if(!(rw.columns.get(firstIndex).data == '*' && rw.columns.get(lastIndex).data == '*')) {
                                            cell.isRequired = true;
                                            if(cell.data == '*'){
                                                isMissingRequired = true;
                                            }
                                        }
                                    //}


                                }
                                rw.columns.add(cell);

                            }

                            //move curser
                            c = currIndex -1;
                        }
                        else{
                            //System.debug('invalid column header');
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid header \'' + columnHeader + '\''));
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'See Template Upload CSV file for guidance'));
                            ApexPages.currentPage().getParameters().put('error', 'errfile');
                            return null;
                        }
                    }//for loop column
                    colRows.add(rw);
                    //System.debug(uRow.theTopic);

                    if(uRow.theTopic.Name != null && uRow.theTopic.Name != ''){
                        uRow.theTopic.put('ExternalID__c', agenda.Name + uRow.theTopic.Name );
                        rw.topicExtId =uRow.theTopic.ExternalID__c;
                    }
                    //System.debug(uRow);
                    uRows.add(uRow);

                }// else statement
                    //System.debug('u row = ' + uRows.size());
            }//for loop row

            //should only query current records for comparison
            //if input from user is valid
            if(!isMissingRequired){
                //query for current data
                //add to maps
                for(UploadRow uRow : uRows){
                    topicsByExtId.put(uRow.theTopic.ExternalID__c, null);
                    //System.debug(uRow.participants);
                    for(String key : uRow.participants.keySet()){
                        participantsByExtId.put(uRow.participants.get(key).ExternalID__c,null );
                    }
                    //for(String key : uRow.personAccounts.keySet()){
                      //  personsByExtId.put(uRow.personAccounts.get(key).ExtID__c, null );
                    //}
                    //for(String key : uRow.topicRoles.keySet()){
                    //    topicRolesByExtId.put(uRow.topicRoles.get(key).ExternalID__c, null);
                    //}

                }
                //System.debug(topicRolesByExtId);
                //System.debug(participantsByExtId);
                //System.debug('Topic' + topicsByExtId);


                for(Topic__c t : Database.query('SELECT '+ String.join(new List<String>(topicFields.keySet()), ',') +' FROM Topic__c WHERE ExternalID__c  IN (\''+ String.join(new List<String>(topicsByExtId.keySet()), '\',\'') + '\') AND Agenda__c = \'' + String.escapeSingleQuotes(agenda.Id) + '\'')){
                    //System.debug(t);
                    topicsByExtId.put(t.ExternalID__c, t);

                }
//                for(String exId : topicsByExtId.keySet()){
//                    if(topicsByExtId.get(exId) == null){
//                        nd.n_topics++;
//                    }
//                    else{
//                        cd.c_topics++;
//                    }
//                }
                for(Participant__c p : [SELECT Name, ExternalID__c, Type__c, Email__c, Salutation__c FROM Participant__c WHERE ExternalID__c IN :participantsByExtId.keySet() AND Agenda__c = :agenda.Id]){
                    //System.debug(p);
                    participantsByExtId.put(p.ExternalID__c, p);
                }
//                for(String exId : participantsByExtId.keySet()){
//                    if(participantsByExtId.get(exId) == null){
//                        nd.n_participants++;
//                    }
//                    else{
//                        cd.c_participants++;
//                    }
//                }
           //     for(Account a : [SELECT Name, ExtID__c, FirstName, LastName, Salutation FROM Account WHERE ExtID__c IN :personsByExtId.keySet()]){
                    //System.debug(a);
              //      personsByExtId.put(a.ExtID__c, a);
              //  }
//                for(String exId : personsByExtId.keySet()){
//                    if(personsByExtId.get(exId) == null){
//                        nd.n_accounts++;
//                    }
//                    else{
//                        cd.c_accounts++;
//                    }
//                }
                
                //System.debug( participantsByExtId);
                for(RowWrapper rw : colRows){
                    for(CellWrapper cw : rw.columns){

                        String columnHeader = colHeaders.get(cw.colNum);
                        if(columnHeader.startsWith('topic_')){

                            //System.debug(rw.topicExtId);
                            //System.debug(topicsByExtId);
                            if(topicsByExtId.containsKey(rw.topicExtId) && topicsByExtId.get(rw.topicExtId) != null){

                                Topic__c oldTopic =  topicsByExtId.get(rw.topicExtId);
                                //System.debug('Old Topic '+oldTopic);

                                String fieldName = columnHeader.substringAfter('topic_');
                                //System.debug(fieldName);
                                if(topicFields.get(fieldName).getDescribe().getType() == DisplayType.DOUBLE){
                                    if(oldTopic.get(fieldName) != null){
                                        if(Double.valueOf(oldTopic.get(fieldName)).format() != cw.data){
                                            cw.isChanged = true;
                                            //cd.c_topics++;
                                            //cw.recordChanged = CellChange.TOPIC;
                                        }
                                    }

                                    //same data if here
                                }
                                else{
                                    //System.debug(oldTopic.get(fieldName));
                                    //System.debug(cw.data);
                                    if((oldTopic.get(fieldName) == null) && cw.data == '*'){
                                        //if both blank, do nothing
                                    }
                                    else{ //if not check if they're different
                                        //System.debug(oldTopic.get(fieldName) + ' ' + cw.data);
                                        if(oldTopic.get(fieldName) != cw.data){
                                            System.debug(cw.data + 'is changed');
                                            cw.isChanged = true;
                                           // cd.c_topics++;
                                            //cw.recordChanged = CellChange.TOPIC;
                                        }
                                    }
                                }
                            }
                            else{
                                System.debug(cw.data);
                                System.debug(topicsByExtId.get(rw.topicExtId));

                                //nd.n_topics++;
                                cw.isNew = true;
                            }
                            //System.debug(cw);


                        }
                        //compare the cell with data related to record before upsert
                        else if(columnHeader.startsWith('Presenter') || columnHeader.startsWith('Reviewer')){

                            //System.debug(personsByExtId.get(cw.accountExtId));
                            //System.debug(cw);
//                            if(personsByExtId.containsKey(cw.accountExtId) && personsByExtId.get(cw.accountExtId) !=null){
//                                Account oldPerson = personsByExtId.get(cw.accountExtId);
//
//                                if(columnHeader.indexOf('First Name') != -1){
//
//                                    if(oldPerson.FirstName != cw.data.trim()){
//                                        if(cw.data != '*'){
//                                            cw.isChanged = true;
//                                            System.debug(oldPerson.FirstName);
//                                            System.debug(cw.data);
//                                        }
//
//
//                                        //System.debug('is changed ' + cw.isChanged);
//                                    }
//                                    //same if here
//                                }
//                                else if(columnHeader.indexOf('Last Name') != -1){
//                                    if(oldPerson.LastName != cw.data.trim()){
//                                        if(cw.data != '*'){
//                                            cw.isChanged = true;
//                                        }
//
//                                    }
//                                }
//                                else if(columnHeader.indexOf('Salutation') != -1){
//                                    if(oldPerson.Salutation != cw.data.trim()){
//                                        if(cw.data != '*'){
//                                            cw.isChanged = true;
//                                        }
//
//                                    }
//                                }
//
//                            }
//                            else{
//
//                                if(cw.data != '*'){
//                                    cw.isNew= true;
//
//                                }
//
//                            }
                            //System.debug(cw);
                            //System.debug(participantsByExtId);

                            if(participantsByExtId.containsKey(cw.participantExtId) && participantsByExtId.get(cw.participantExtId) != null){
                                Participant__c oldParticipant = participantsByExtId.get(cw.participantExtId);
                                //System.debug(oldParticipant.Type__c);
                                if(columnHeader.indexOf('Speaker') != -1){
                                    if(oldParticipant.Type__c != UtilConstants.Presenter){//changed role
                                        cw.isChanged = true;

                                        //System.debug(cw);
                                    }
                                }
                                else if(columnHeader.indexOf('Reviewer') != -1){
                                    if(oldParticipant.Type__c != UtilConstants.NonPresenter){//changed role
                                        cw.isChanged = true;

                                        //System.debug(cw);
                                    }
                                }

                                else if(columnHeader.indexOf('First Name') != -1){

                                    if(oldParticipant.Name.substringBefore(' ') != cw.data.trim()){
                                        if(cw.data != '*'){
                                            cw.isChanged = true;
                                            System.debug(oldParticipant.Name);
                                            System.debug(cw.data);
                                        }


                                        //System.debug('is changed ' + cw.isChanged);
                                    }
                                    //same if here
                                }
                                else if(columnHeader.indexOf('Last Name') != -1){
                                    if(oldParticipant.Name.substringAfter(' ') != cw.data.trim()){
                                        if(cw.data != '*'){
                                            cw.isChanged = true;
                                        }

                                    }
                                }
                                else if(columnHeader.indexOf('Salutation') != -1){
                                    if(oldParticipant.Salutation__c != cw.data.trim()){
                                        if(cw.data != '*'){
                                            cw.isChanged = true;
                                        }

                                    }
                                }
                            }
                            else{
                                //System.debug('set new');
                                //System.debug(cw);
                                if(cw.data != '*'){
                                    cw.isNew = true;

                                }

                            }
                            //possibly do not need...
                           // if(topicRolesByExtId.containsKey(cw.topicRoleExtId) == true){

                           // }

                        }
                    }

                }


            }

            //System.debug(colRows.get(1).size());
        }
        catch(Exception e){
            System.debug(e.getStackTraceString());
            ApexPages.addMessages(e);
        }

        Next();
        return null;
    }
    public void commitData(){

        Map<String, Topic__c> topicsToUpsert = new Map<String, Topic__c>();
       // Map<String,Account> pplAccountsToUpsert = new  Map<String,Account>();
        //Map<String, Topic_Role__c> topicRolesToUpsert = new Map<String, Topic_Role__c>();
        Map<String,Participant__c> participantsToUpsert =  new Map<String,Participant__c>();
        Map<String, Disclosure__c> disclosersToUpsert = new Map<String, Disclosure__c>();

        Savepoint sp = Database.setSavepoint();
        try{

            for(UploadRow ur : uRows){
                //System.debug(ur.personAccounts);
                topicsToUpsert.put(ur.theTopic.ExternalID__c,ur.theTopic);
//                for(String key: ur.personAccounts.keySet()){
//                    pplAccountsToUpsert.put(key, ur.personAccounts.get(key));
//                }
            }

            //System.debug(pplAccountsToUpsert);
            upsert topicsToUpsert.values() ExternalID__c;
          //  upsert pplAccountsToUpsert.values() ExtID__c;

            for(UploadRow ur : uRows){

                for(String key: ur.participants.keySet()){
                    ur.participants.get(key).topic__c = ur.theTopic.id;
                   // ur.participants.get(key).participant__c = pplAccountsToUpsert.get(key).id;
                    participantsToUpsert.put(ur.participants.get(key).ExternalID__c,ur.participants.get(key));

                }

            }
            //System.debug(participantsToUpsert);
            //upsert topicRolesToUpsert.values() ExternalID__c;
            upsert participantsToUpsert.values() ExternalID__c;
        }
        catch(Exception e){
            Database.rollback(sp);
            System.debug(e.getStackTraceString());
            ApexPages.addMessages(e);
        }


        //advance panel
        Next();

    }
//    public Integer getTotalRecordChanges(){
//        return cd.total() + nd.total();
//    }
    /*Panel navigation methods*/
    public void Back(){
        if(currentStep > 0)
            currentStep= currentStep -1;
    }
   
    public void Next() {

        currentStep = currentStep + 1;
    }
    public PageReference Cancel(){
        PageReference pageRef = new PageReference('/'+agenda.id);
        return pageRef.setRedirect(true);
    }

    /*Utility function for converting blobs to strings*/
     public static String blobToString(Blob input, String inCharset){
            String hex = EncodingUtil.convertToHex(input);
            System.assertEquals(0, hex.length() & 1);
            final Integer bytesCount = hex.length() >> 1;

            String[] bytes = new String[bytesCount];
            for(Integer i = 0; i < bytesCount; ++i)
                bytes[i] =  hex.mid(i << 1, 2);
            return EncodingUtil.urlDecode('%' + String.join(bytes, '%'), inCharset);
     }

    /*Wrapper class for rows to upload*/
    public class UploadRow{
        public Topic__c theTopic{get;set;}
       // public Map<String,Topic_Role__c> topicRoles{get;set;}
       // public Map<String,Account> personAccounts{get;set;}
        public Map<String,Participant__c> participants{get;set;}

    }

}