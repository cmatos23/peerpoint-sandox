public without  sharing class AgendaManagerController {

    public Agenda__c agenda{get;set;}
    public Integer participantRowNumber{get;set;}
    public List<ParticipantWrapper> participantWrappers{get;set;}
    public List<ParticipantWrapper> particpantWrappersForDeletion{get;set;}
    public Map<String, Participant__c> participantsByEmailAddress{get;set;}
    public Integer currentIndex{get;set;}
    public List<SelectOption> reviewerOptions{get;set;}
    public Map<Id, List<Topic_Presenter__c>> presentersByParticipantId {get;set;}
    public AgendaManagerController(ApexPages.StandardController controller){
        participantWrappers = new List<ParticipantWrapper>();
        particpantWrappersForDeletion = new List<ParticipantWrapper>();
        participantsByEmailAddress = new Map<String, Participant__c>();
        presentersByParticipantId = new Map<Id, List<Topic_Presenter__c>>();
        agenda = (Agenda__c)controller.getRecord();
        String agendaId;
        if(agenda == null){
            if (ApexPages.currentPage().getParameters().get('id') == null) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Must provide an Activity to manage'));
                ApexPages.currentPage().getParameters().put('error', 'noparam');
                return;
            }
            agendaId = ApexPages.currentPage().getParameters().get('id');
        }
        else{
            agendaId = agenda.id;
        }
        agenda =  [SELECT id, Name FROM Agenda__c WHERE id = :agendaId];
        List<Participant__c> participants = [SELECT id, Name, Salutation__c, Email__c, Type__c, Projected_CME_Hours__c, Topic__c,Topic__r.Name, Participant__c
                                                        FROM Participant__c WHERE Agenda__c = :agenda.id];
        Map<Id, Participant__c> participantsByIds = new Map<Id, Participant__c>();

        List<Participant__c> presenters = new List<Participant__c>();
        List<Participant__c> nonPresenters = new List<Participant__c>();
        for(Participant__c participant : participants){
            if(participant.Type__c != null){

                if(participant.Type__c.contains(UtilConstants.Presenter)){
                    presenters.add(participant);
                }
                if(participant.Type__c != UtilConstants.Presenter  && !participant.Type__c.contains(UtilConstants.PresenterUnavailable)){
                    nonPresenters.add(participant);
                }

                if(!participantsByEmailAddress.containsKey(participant.Email__c) && !participant.Type__c.contains(UtilConstants.PresenterUnavailable) ){
                    participantsByEmailAddress.put(participant.Email__c, participant);
                }
            }


        }
        System.debug(presenters);
        System.debug(nonPresenters);
        List<Topic_Presenter__c> topicPresenters = [SELECT id, Topic__c, Presenter__c, Presenter__r.Email__c FROM Topic_Presenter__c WHERE Presenter__c IN :presenters];

        for(Topic_Presenter__c tp : topicPresenters){
            if(!presentersByParticipantId.containsKey(tp.Presenter__c)){
                presentersByParticipantId.put(tp.Presenter__c, new List<Topic_Presenter__c>());
            }
            presentersByParticipantId.get(tp.Presenter__c).add(tp);
        }
        System.debug(participants);
        currentIndex = 0;
        reviewerOptions = new List<SelectOption>();
        reviewerOptions.add( new SelectOption('', '--None--'));
        List<Id> participantIds = new List<Id>();
        for(Participant__c participant : participants) {
          //  accountIds.add(participant.Participant__c);
            participantIds.add(participant.id);
            if(participant.Type__c != null && !participant.Type__c.contains(UtilConstants.Presenter)){
                SelectOption option = new SelectOption( participant.id, participant.Name + ' - ' + participant.Type__c);
                reviewerOptions.add(option);
            }

        }

        Map<Id, Disclosure__c> disclosuresByParticipantIds = new Map<Id, Disclosure__c>();
        Map<Id, Disclosure__c> disclosuresByIds = new Map<Id, Disclosure__c>();
        for(Disclosure__c d : [SELECT id, Name, Status__c, Participant__c, COI_Reviewer__c FROM Disclosure__c WHERE Participant__c IN :participantIds AND Agenda__c = :agenda.id]){
            if(!disclosuresByParticipantIds.containsKey(d.Participant__c)){
                disclosuresByParticipantIds.put(d.Participant__c, d);
            }
            if(!disclosuresByIds.containsKey(d.id)){
                disclosuresByIds.put(d.id, d);
            }
        }
        Map<Id, COI__c> coisByParticipantIds = new Map<Id, COI__c>();

        for(COI__c c : [SELECT id, Status__c, Disclosure__c, Disclosure__r.Participant__c FROM COI__c WHERE Disclosure__c IN :disclosuresByIds.keySet()]){
            if(!coisByParticipantIds.containsKey(c.Disclosure__r.Participant__c)){
                coisByParticipantIds.put(c.Disclosure__r.Participant__c, c);
            }
        }

        Map<Id, Topic__c> topicsByIds = new Map<Id, Topic__c>([SELECT id, Name, Projected_CME_Hours__c FROM Topic__c WHERE Agenda__c = :agenda.id]);

        for(Topic_Presenter__c tp : topicPresenters){
            ParticipantWrapper pw = new ParticipantWrapper(tp, participantsByEmailAddress.get(tp.Presenter__r.Email__c), disclosuresByParticipantIds.get(tp.Presenter__c), topicsByIds.get(tp.Topic__c), coisByParticipantIds.get(tp.Presenter__c));
            pw.index = currentIndex;
            currentIndex++;
            participantWrappers.add(pw);
        }

        for(Participant__c p : nonPresenters){

            ParticipantWrapper pw =  new ParticipantWrapper(null, p, disclosuresByParticipantIds.get(p.id), topicsByIds.get(p.Topic__c), coisByParticipantIds.get(p.id));
            pw.index = currentIndex;
            System.debug(pw);
            currentIndex++;
            participantWrappers.add(pw);


        }

        System.debug(participantWrappers);

    }

    public PageReference save(){

        System.debug(particpantWrappersForDeletion);
        deleteWrappers();

        Map<String, Topic__c> topicsByName = new Map<String, Topic__c>();
        Map<String, Account> accountsByEmail = new Map<String, Account>();
        List<Participant__c> participantsToUpdate = new List<Participant__c>();
        List<Participant__c> participantsToInsert = new List<Participant__c>();
        List<Disclosure__c> disclosuresToUpsert = new List<Disclosure__c>();
        List<Topic_Presenter__c> topicPresentersToUpsert = new List<Topic_Presenter__c>();

        System.debug(participantWrappers);
        for(ParticipantWrapper pw : participantWrappers){

            if(wrapperValidation(pw) == false){
                return null;
            }

            if(pw.participant.Type__c != null && (pw.participant.Type__c.contains(UtilConstants.Presenter) || pw.participant.Type__c == UtilConstants.PresenterUnavailable)){
                if(!topicsByName.containsKey(pw.relatedTopic.Name)){
                    topicsByName.put(pw.relatedTopic.Name, pw.relatedTopic);
                }
            }

            if(pw.relatedDisclosure.Status__c == UtilConstants.PositiveDisclosure){
                disclosuresToUpsert.add(pw.relatedDisclosure);
            }
            if(!participantsByEmailAddress.containsKey(pw.participant.Email__c)){
                participantsByEmailAddress.put(pw.participant.Email__c, pw.participant);
            }

        }
        System.debug(participantsByEmailAddress);
        System.debug(topicsByName);
        for(Topic__c topic : [SELECT id, Name, Projected_CME_Hours__c FROM Topic__c WHERE Name IN :topicsByName.keyset() AND Agenda__c = :agenda.id]){

            topic.Projected_CME_Hours__c = topicsByName.get(topic.Name).Projected_CME_Hours__c;
            topicsByName.put(topic.Name, topic);


        }

        //System.debug(topicsByName.keySet());
        for(String key : topicsByName.keySet()){
            if(topicsByName.get(key).id == null){
                topicsByName.get(key).Agenda__c = agenda.id;
            }
        }
        Savepoint sp = Database.setSavepoint();
        try{
           // System.debug(accountsByEmail.values());
            System.debug(topicsByName.values());
            upsert topicsByName.values();

            Set<Participant__c> setParticipants = new Set<Participant__c>();
//            for(ParticipantWrapper pw : participantWrappers){
//                if(!participantsByEmailAddress.containsKey(pw.participant.Email__c)){
//                    participantsByEmailAddress.put(pw.participant.Email__c, pw.participant);
//                }
//            }
            for(Participant__c participant : participantsByEmailAddress.values()){
                setParticipants.add(participant);
            }
            List<Participant__c> participantsList = new List<Participant__c>();
            participantsList.addAll(setParticipants);
            upsert participantsList;

            for(ParticipantWrapper pw : participantWrappers){
                if(pw.participant.Type__c != null && pw.participant.Type__c.contains(UtilConstants.Presenter)){
                    System.debug(pw.relatedTopicPresenter);
                    if(!pw.participant.Type__c.contains(UtilConstants.PresenterUnavailable)){
                        pw.relatedTopicPresenter.Presenter__c = participantsByEmailAddress.get(pw.participant.Email__c).id;
                    }
                    pw.relatedTopicPresenter.Topic__c = topicsByName.get(pw.relatedTopic.Name).id;
                    topicPresentersToUpsert.add(pw.relatedTopicPresenter);
                }
            }
            upsert topicPresentersToUpsert;

            upsert disclosuresToUpsert;

            List<Participant__c> participantsToDelete = new List<Participant__c>();
            List<Topic_Presenter__c> topicPresentersToDelete = new List<Topic_Presenter__c>();
            for(Participant__c participant : [SELECT id , Type__c, (SELECT id FROM Topic_Presenters__r)FROM Participant__c WHERE Agenda__c = :agenda.id]){
                if(participant.Topic_Presenters__r.size() == 0 && participant.Type__c.contains(UtilConstants.Presenter)){
                    participantsToDelete.add( participant);
                }
                if(participant.Topic_Presenters__r.size() > 0 && !participant.Type__c.contains(UtilConstants.Presenter) ){
                    topicPresentersToDelete.addAll(participant.Topic_Presenters__r);
                }
            }

            delete topicPresentersToDelete;

            delete participantsToDelete;

        }
        catch(Exception e){
            Database.rollback(sp);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'There was an error processing your request.'));
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            System.debug(e.getStackTraceString());
            System.debug(e.getMessage());
            return null;
        }


        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Records Saved!'));
        return null;
    }
    public void deleteWrappers(){
        Map<Id, Participant__c> participantsToDelete = new Map<Id, Participant__c>();
        List<Topic_Presenter__c> topicPresentersToDelete = new List<Topic_Presenter__c>();
        for(ParticipantWrapper pw : particpantWrappersForDeletion){
            if(pw.participant != null && pw.participant.id != null){
                if( !pw.participant.Type__c.contains(UtilConstants.Presenter)) {
                    participantsToDelete.put(pw.participant.id, pw.participant);
                }
                else if(pw.isTopicPresenter == true){
                    topicPresentersToDelete.add(pw.relatedTopicPresenter);
                }
            }

        }
        try{
            delete topicPresentersToDelete;



            delete participantsToDelete.values();
            particpantWrappersForDeletion.clear();
        }
        catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'There was an error attempting to delete a Participant.'));
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
        }
    }
    public Boolean wrapperValidation(ParticipantWrapper wrapper){

        if(wrapper.participant == null || wrapper.relatedDisclosure == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error Saving Records'));
            System.debug(wrapper);
            return false;
        }
        if(wrapper.participant.Type__c != UtilConstants.PresenterUnavailable){
            if(String.isBlank(wrapper.participant.Name)){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Participants must have a Name!'));
                return false;
            }
            if(String.isBlank(wrapper.participant.Email__c)){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Participants must have an Email Address!'));
                return false;
            }
        }

        if(wrapper.participant.Type__c != null && wrapper.participant.Type__c.contains(UtilConstants.Presenter)  || wrapper.participant.Type__c == UtilConstants.PresenterUnavailable){
            if(String.isBlank(wrapper.relatedTopic.Name)){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Missing a Topic Title!'));
                System.debug(wrapper.relatedTopic);
                return false;
            }
        }
        return true;
    }
    public void addParticipantWrapper(){
        ParticipantWrapper pw = new ParticipantWrapper(agenda.id);
        pw.index = currentIndex;
        currentIndex++;
        participantWrappers.add(pw);
    }
    public void removeParticipant(){
        if(participantRowNumber < 0 || participantRowNumber >= participantWrappers.size()){
            System.debug('invalid index in removeParticipant: ' + participantRowNumber);
            return;
        }
        if(participantWrappers.get(participantRowNumber) == null ){
            System.debug('Error on removeParticipant: row attempted to delete - ' + participantRowNumber + ', size of list - ' + participantWrappers.size());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error removing row ' + participantRowNumber));
            //ApexPages.currentPage().getParameters().put('error', 'rowerr');
            return;
        }
        if(participantWrappers.get(participantRowNumber).participant.id != null){

            particpantWrappersForDeletion.add(participantWrappers.get(participantRowNumber));


        }
        if(participantsByEmailAddress.get(participantWrappers.get(participantRowNumber).participant.Email__c) != null){
            participantsByEmailAddress.remove(participantWrappers.get(participantRowNumber).participant.Email__c);
        }

        participantWrappers.remove(participantRowNumber);
        currentIndex--;
        resetIndexes();
    }
    public void resetIndexes(){
        for(Integer i = 0; i < participantWrappers.size(); i++){
            participantWrappers[i].index = i;
        }
    }

    public class ParticipantWrapper{

       // public Account relatedAccount{get;set;}
        public Participant__c participant{get;set;}
        public Disclosure__c relatedDisclosure{get;set;}
        public Topic__c relatedTopic{get;set;}
        public Topic_Presenter__c relatedTopicPresenter{get;set;}
        public String coiStatus{get;set;}
        public Integer index{get;set;}
        public Boolean isTopicPresenter;

        public ParticipantWrapper(Topic_Presenter__c topicPresenter, Participant__c participant, Disclosure__c disclosure, Topic__c topic, COI__c coi){
            this.participant = participant == null ? new Participant__c() : participant;
            this.relatedDisclosure = disclosure == null ? new Disclosure__c() : disclosure;
            this.relatedTopic = topic == null ? new Topic__c() : topic;
            this.coiStatus = coi == null ? '' : coi.Status__c;
            if(topicPresenter == null){
                relatedTopicPresenter = new Topic_Presenter__c();
                isTopicPresenter = false;
            }
            else{
                relatedTopicPresenter = topicPresenter;
                isTopicPresenter = true;
            }


            index = -1;
        }
        public ParticipantWrapper(String agendaId){
            this.participant = new Participant__c();
            this.relatedDisclosure = new Disclosure__c();
            this.relatedTopic = new Topic__c();
            this.relatedTopicPresenter = new Topic_Presenter__c();
            this.participant.Agenda__c = agendaId;
            this.coiStatus = '';
        }

    }
}