@isTest
public class TestSiteDisclosureFormController {

    static testMethod void testForm() {
        Account host = new Account(Name = 'HostAccount');
        insert host;

        Agenda__c agenda = new Agenda__c(Name = 'Agenda', Account__c = host.Id);
        insert agenda;

        Topic__c topic = new Topic__c(Name = 'Topic', Agenda__c = agenda.Id);
        insert topic;

        Participant__c participant = new Participant__c(Topic__c = topic.Id, Agenda__c = agenda.Id, Name = 'Speaker');
        insert participant;

        Disclosure__c disclosure = new Disclosure__c();
        disclosure.Participant__c = participant.Id;
        disclosure.Agenda__c = agenda.Id;
        insert disclosure;

        PageReference thisPage = Page.SiteDisclosureForm;
        thisPage.getParameters().put('dId', disclosure.Id);
        Test.setCurrentPage(thisPage);
        SiteDisclosureFormController siteDisclosureFormController = new SiteDisclosureFormController();
        siteDisclosureFormController.checkIfSubmitted();
        siteDisclosureFormController.disclosure.No__c = true;
        siteDisclosureFormController.save();

        siteDisclosureFormController.discRelWrappers.get(0).discRel.Commercial_Interest__c = 'Interest';
        siteDisclosureFormController.discRelWrappers.get(0).discRel.For_What_Role__c = 'Role';
        siteDisclosureFormController.discRelWrappers.get(0).discRel.What_was_received__c = 'Received';

        siteDisclosureFormController.disclosure.Level_of_participation_in_the_activity__c = 'Presenter';
        siteDisclosureFormController.disclosure.Acknowledgement__c = true;
        siteDisclosureFormController.disclosure.Full_Legal_Name__c = 'My Legal Name';
        siteDisclosureFormController.disclosure.Sign_Date__c = Date.today();
        siteDisclosureFormController.disclosure.No__c = false;

        siteDisclosureFormController.save();
        siteDisclosureFormController.addRel();

        siteDisclosureFormController.discRelWrappers.get(1).discRel.Commercial_Interest__c = 'Interest';
        siteDisclosureFormController.discRelWrappers.get(1).discRel.For_What_Role__c = 'Role';
        siteDisclosureFormController.discRelWrappers.get(1).discRel.What_was_received__c = 'Received';

        siteDisclosureFormController.addRel();
        siteDisclosureFormController.idToRemove = 2;
        siteDisclosureFormController.removeRel();
        siteDisclosureFormController.submit();

        disclosure = [Select id, Participant__r.Name, (Select id, Commercial_Interest__c, For_What_Role__c, What_was_received__c from Disclosure_Relationships__r) from Disclosure__c where id = :disclosure.id];
        System.assertEquals(2, disclosure.Disclosure_Relationships__r.size());
        System.assert(disclosure.Participant__r.Name == 'Speaker');

        thisPage = Page.SiteDisclosureForm;
        thisPage.getParameters().put('dId', disclosure.Id);
        Test.setCurrentPage(thisPage);
        siteDisclosureFormController = new SiteDisclosureFormController();
        siteDisclosureFormController.checkIfSubmitted();
    }
}