/**
 * Created by ChrisMatos on 8/25/2016.
 */

@IsTest
public with sharing class ActivityManagerControllerTest {
    @TestSetup
    public static void setup(){
        Account a = new Account(Name = 'TestAccount');
        insert a;

        Agenda__c ag = new Agenda__c(Name = 'TestActivity', Account__c = a.id);
        insert ag;

        Topic__c t = new Topic__c(Agenda__c=ag.id, Name='Test Topic1');
        insert t;

        Participant__c p = new Participant__c(Email__c = 'uniqueTestEmail@test.test', Last_Name__c = 'Test One');
        insert p;

        Topic_Presenter__c tp = new Topic_Presenter__c(Presenter__c = p.id, Topic__c = t.id, Agenda__c = ag.id, Role__c = UtilConstants.NonPresenter);
        insert tp;


    }

    public static testMethod void itShouldGetExistingTopicPresentersOnActivity(){
        Agenda__c activity = [SELECT id, Name FROM Agenda__c LIMIT 1];

        Test.setCurrentPageReference(new PageReference('Page.AgendaManager'));
        ApexPages.currentPage().getParameters().put('id', activity.id);

        ActivityManagerController controller = new ActivityManagerController();

        System.assertEquals(1, controller.topicPresenterWrappers.size());
    }
    public static testMethod void itShouldCreateNewSpeaker(){
        Agenda__c activity = [SELECT id, Name FROM Agenda__c LIMIT 1];
        Participant__c part = [SELECT id, Email__c, Last_Name__c FROM Participant__c LIMIT 1];
        Test.setCurrentPageReference(new PageReference('Page.AgendaManager'));
        ApexPages.currentPage().getParameters().put('id', activity.id);

        ActivityManagerController controller = new ActivityManagerController();


        controller.addTopicPresenterWrapper();
        System.assertEquals(2, controller.topicPresenterWrappers.size());//one already exists, plus new one

        ActivityManagerController.TopicPresenterWrapper pw = controller.topicPresenterWrappers.get(1);

        String newEmail = part.Email__c + 'TEST';
        String newTopic = 'Test Topic2';

        pw.participant.Last_Name__c = 'Test Two';
        pw.topicPresenter.Role__c = UtilConstants.Presenter;
        pw.participant.Email__c = 'ChrisMatos@test.com';
        pw.topic.Name = newTopic;

        controller.save();

        List<Topic__c> topics = [SELECT id, Name From Topic__c Where Name = :newTopic];
        System.assertEquals(1, topics.size());
        List<Participant__c> parts = [SELECT id, Name, Email__c From Participant__c WHERE Last_Name__c LIKE 'Test%'];
        System.assertEquals(2, parts.size());

    }
    public static testMethod void itShouldPerformDeletion(){
        Agenda__c activity = [SELECT id, Name FROM Agenda__c LIMIT 1];

        Test.setCurrentPageReference(new PageReference('Page.AgendaManager'));
        ApexPages.currentPage().getParameters().put('id', activity.id);
        ActivityManagerController controller = new ActivityManagerController();

        controller.wrapperRowNumber = 0;
        controller.removeTopicPresenterWrapper();

        controller.save();

        List<Participant__c> participants = [SELECT id From Participant__c];
        System.assertEquals(0, participants.size());
    }
}