/**
 * Created by ChrisMatos on 10/18/2016.
 */

public with sharing class TopicPresenterTriggerService {
    public static void handleAfterUpdate(Map<Id, Topic_Presenter__c> newMap, Map<Id, Topic_Presenter__c> oldMap){
        Map<Id, Topic_Presenter__c> topicPresentersByIds = new Map<id, Topic_Presenter__c>();
        for(Id key : newMap.keySet()) {
            if (newMap.get(key).Role__c != oldMap.get(key).Role__c && newMap.get(key).Role__c == UtilConstants.Presenter) {
                topicPresentersByIds.put(key, newMap.get(key));
            }
        }
        generateDisclosures(topicPresentersByIds);
    }
    public static void handleAfterInsert(Map<Id, Topic_Presenter__c> newMap) {
        Map<Id, Topic_Presenter__c> topicPresentersByIds = new Map<id, Topic_Presenter__c>();
        for(Id key : newMap.keySet()) {
            if (newMap.get(key).Role__c != null && newMap.get(key).Role__c == UtilConstants.Presenter && newMap.get(key).Presenter__c != null) {
                topicPresentersByIds.put(key, newMap.get(key));
            }
        }
        generateDisclosures(topicPresentersByIds);
    }
    public static void generateDisclosures(Map<Id, Topic_Presenter__c> topicPresentersByIds){
        Map<Id, Participant__c> participantsByIds = new Map<Id, Participant__c>();

        for(Id key : topicPresentersByIds.keySet()){
            if(!participantsByIds.containsKey(topicPresentersByIds.get(key).Presenter__c)){
                participantsByIds.put(topicPresentersByIds.get(key).Presenter__c, null);
            }
        }

        Map<Id, Disclosure__c> disclosuresByParticipantIds = new Map<Id, Disclosure__c>();

        List<Disclosure__c> disclosures = [SELECT id, Participant__c FROM Disclosure__c WHERE Participant__c IN :participantsByIds.keySet()];
        for(Disclosure__c disclosure : disclosures){
            if(!disclosuresByParticipantIds.containsKey(disclosure.Participant__c)){
                disclosuresByParticipantIds.put(disclosure.Participant__c, disclosure);
            }
        }
        List<Disclosure__c> disclosuresToInsert = new List<Disclosure__c>();
        for(Topic_Presenter__c tp : topicPresentersByIds.values()){
            if(!disclosuresByParticipantIds.containsKey(tp.Presenter__c)){
                Disclosure__c disclosure = new Disclosure__c();
                disclosure.Agenda__c = tp.Agenda__c;
                disclosure.Level_of_participation_in_the_activity__c = tp.Role__c;
                disclosure.Participant__c = tp.Presenter__c;
                disclosure.Status__c = 'Sent';

                disclosuresToInsert.add(disclosure);
            }
        }

        System.debug(disclosuresToInsert);
        try{
            insert disclosuresToInsert;
        }
        catch(Exception e){
            System.debug(e.getStackTraceString());
            System.debug(e.getMessage());
        }


    }
}