@isTest
public with sharing class TestSiteCOIFormController {

    @testSetup static void setup() {
        Account host = new Account(Name = 'HostAccount');
        insert host;
        Account speaker = new Account(Name = 'Speaker');
        insert speaker;

        Agenda__c agenda = new Agenda__c(Name = 'Agenda', Account__c = host.Id);
        insert agenda;

        Topic__c topic = new Topic__c(Name = 'Topic', Agenda__c = agenda.Id);
        insert topic;

        Participant__c participant = new Participant__c(Participant__c = speaker.Id, Topic__c = topic.Id, Agenda__c = agenda.Id);
        insert participant;

        Disclosure__c disclosure = new Disclosure__c(Agenda__c = agenda.Id, Participant__c = participant.Id,
                Level_of_participation_in_the_activity__c = 'Presenter', No__c = true, In_compliance_with_HIPAA__c = true);
        insert disclosure;

        Disclosure_Relationship__c discRel = new Disclosure_Relationship__c();
        discRel.Commercial_Interest__c = 'Interest';
        discRel.For_What_Role__c = 'Role';
        discRel.What_Was_Received__c = 'Received';
        discRel.Disclosure__c = disclosure.Id;
        insert discRel;
    }

    static testMethod void testApprovedForm() {
        Disclosure__c disclosure = [Select id from Disclosure__c limit 1];
        PageReference thisPage = Page.SiteCOIForm;
        thisPage.getParameters().put('id', disclosure.Id);
        Test.setCurrentPage(thisPage);
        SiteCOIFormController siteCOIFormController = new SiteCOIFormController();

        siteCOIFormController.approvedCheck = true;
        siteCOIFormController.coi.Comments__c = 'These are some comments';
        siteCOIFormController.save();

        COI__c coi = [Select id, CME_Content_approved_or_denied__c, Comments__c, Disclosure__c, Status__c from COI__c where id = :siteCOIFormController.coi.Id];
        System.assert(coi.Disclosure__c == disclosure.Id);
        System.assertEquals('Approved', coi.CME_Content_approved_or_denied__c);
        System.assertEquals('These are some comments', coi.Comments__c);
    }

    static testMethod void testDeniedForm() {
        Disclosure__c disclosure = [Select id from Disclosure__c limit 1];
        PageReference thisPage = Page.SiteCOIForm;
        thisPage.getParameters().put('id', disclosure.Id);
        Test.setCurrentPage(thisPage);
        SiteCOIFormController siteCOIFormController = new SiteCOIFormController();

        siteCOIFormController.declinedCheck = true;
        siteCOIFormController.coi.Comments__c = 'These are some comments';
        siteCOIFormController.save();

        COI__c coi = [Select id, CME_Content_approved_or_denied__c, Comments__c, Disclosure__c, Status__c from COI__c where id = :siteCOIFormController.coi.Id];
        System.assert(coi.Disclosure__c == disclosure.Id);
        System.assert(coi.CME_Content_approved_or_denied__c == 'Denied');
        System.assert(coi.Comments__c == 'These are some comments');
    }

    static testMethod void testNoAction() {
        Disclosure__c disclosure = [Select id from Disclosure__c limit 1];
        PageReference thisPage = Page.SiteDisclosureForm;
        thisPage.getParameters().put('id', disclosure.Id);
        Test.setCurrentPage(thisPage);
        SiteCOIFormController siteCOIFormController = new SiteCOIFormController();

        siteCOIFormController.save();
        System.assert(ApexPages.getMessages().size() > 0);
    }
}