/**
 * Created by ChrisMatos on 8/25/2016.
 */

@IsTest
public with sharing class AgendaManagerControllerTest {
    @TestSetup
    public static void setup(){
        Account a = new Account(Name = 'TestAccount');
        insert a;
        
        Agenda__c ag = new Agenda__c(Name = 'TestActivity', Account__c = a.id);
        insert ag;
 
        Topic__c t = new Topic__c(Agenda__c=ag.id, Name='Test Topic1');
        insert t;
        
        Participant__c p = new Participant__c(Agenda__c = ag.id, Topic__c = t.id, Email__c = 'uniqueTestEmail@test.test', Name = 'Test One');
        insert p;
        
        
    }

    public static testMethod void itShouldGetExistingParticipantsOnActivity(){
		Agenda__c activity = [SELECT id, Name FROM Agenda__c LIMIT 1];
        
        AgendaManagerController controller = new AgendaManagerController(new ApexPages.StandardController(activity));
        
        System.assertEquals(1, controller.participantWrappers.size());
    }
    public static testMethod void itShouldCreateNewSpeaker(){
        Agenda__c activity = [SELECT id, Name FROM Agenda__c LIMIT 1];
        Participant__c part = [SELECT id, Email__c, Name FROM Participant__c LIMIT 1];
        AgendaManagerController controller = new AgendaManagerController(new ApexPages.StandardController(activity));
        
        controller.addParticipantWrapper();
        System.assertEquals(2, controller.participantWrappers.size());//one already exists, plus new one
        
        AgendaManagerController.ParticipantWrapper pw = controller.participantWrappers.get(1);
        
        String newEmail = part.Email__c + 'TEST';
        String newTopic = 'Test Topic2';
       
        pw.participant.Name = 'Test Two';
        pw.participant.Type__c = UtilConstants.Presenter;
        pw.participant.Email__c = 'ChrisMatos@test.com';
        pw.relatedTopic.Name = newTopic;
        
        controller.save();
        
        List<Topic__c> topics = [SELECT id, Name From Topic__c Where Name = :newTopic];
        System.assertEquals(1, topics.size());
        List<Participant__c> parts = [SELECT id, Name, Email__c From Participant__c WHERE Name LIKE 'Test%'];
        System.assertEquals(2, parts.size());
        
    }
    public static testMethod void itShouldPerformDeletion(){
        Agenda__c activity = [SELECT id, Name FROM Agenda__c LIMIT 1];
        AgendaManagerController controller = new AgendaManagerController(new ApexPages.StandardController(activity));
        controller.participantRowNumber = 0;
        controller.removeParticipant();
        
        controller.save();
        
        List<Participant__c> participants = [SELECT id From Participant__c];
        System.assertEquals(0, participants.size());
    }
}