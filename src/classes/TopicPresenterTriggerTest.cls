/**
 * Created by ChrisMatos on 10/19/2016.
 */

@IsTest
public with sharing class TopicPresenterTriggerTest {
    @TestSetup
    public static void testSetup(){
        Account a = new Account();
        a.Name = 'Test Account';

        insert a;

        Agenda__c agenda = new Agenda__c();
        agenda.Name = 'Test Agenda';
        agenda.Account__c = a.id;

        insert agenda;

        Topic__c topic = new Topic__c();
        topic.Name = 'Test Topic';
        topic.Agenda__c = agenda.id;

        insert topic;
    }
    public static testMethod void itShouldGenerateDisclosureIfNewParticipant(){
		
        Topic__c topic = [SELECT id, Name, Agenda__c FROM Topic__c WHERE Name = 'Test Topic' LIMIT 1];
		
        Participant__c participant = new Participant__c();
        participant.Name = 'Bob Test';
        participant.Email__c = 'bobtest@test.com';
        insert participant;

        Topic_Presenter__c tp = new Topic_Presenter__c();
        tp.Presenter__c = participant.id;
        tp.Topic__c = topic.id;
        tp.Role__c = UtilConstants.NonPresenter;
		tp.Agenda__c = topic.Agenda__c;
        insert tp;


        List<Disclosure__c> disclosures = [SELECT id, Name FROM Disclosure__c];
        System.assertEquals(0, disclosures.size());

        tp.Role__c = UtilConstants.Presenter;
        update tp;

        disclosures = [SELECT id, Name FROM Disclosure__c];
        System.assertEquals(1, disclosures.size());
    }
    public static testMethod void itShouldNotGenerateDisclosureIfParticipantEmailExistsOnActivity(){
        Agenda__c agenda = [SELECT id, Name FROM Agenda__c WHERE Name = 'Test Agenda' LIMIT 1];

        Topic__c topic = [SELECT id, Name, Agenda__c FROM Topic__c WHERE Name = 'Test Topic' LIMIT 1];

        Topic__c topic2 = new Topic__c();
        topic2.Name = 'Test Topic2';
        topic2.Agenda__c = agenda.id;

        insert topic2;


        Participant__c participant = new Participant__c();
        participant.Email__c = 'test@test.com';
        participant.Name = 'John Test';
        insert participant;

        Topic_Presenter__c topicPresenter = new Topic_Presenter__c();
        topicPresenter.Role__c = UtilConstants.Presenter;
        topicPresenter.Topic__c = topic.id;
        topicPresenter.Presenter__c = participant.id;
        topicPresenter.Agenda__c = agenda.id;
        insert topicPresenter;

        List<Disclosure__c> disclosures = [SELECT id, Name FROM Disclosure__c WHERE Participant__r.Email__c = :participant.Email__c];
        System.assertEquals(1, disclosures.size());

        Participant__c participant2 = new Participant__c();
        participant2.Email__c = 'test@test.com';
        participant2.Name = 'John Test';
        insert participant2;

        Topic_Presenter__c topicPresenter2 = new Topic_Presenter__c();
        topicPresenter2.Topic__c = topic2.id;
        topicPresenter2.Agenda__c = topic2.Agenda__c;
        topicPresenter2.Role__c = UtilConstants.Presenter;
        topicPresenter2.Agenda__c = agenda.id;
        insert topicPresenter2;


        disclosures = [SELECT id, Name FROM Disclosure__c WHERE Participant__r.Email__c = :participant.Email__c];
        System.assertEquals(1, disclosures.size());
    }
}