/**
 * Created by ChrisMatos on 10/17/2016.
 */

public with sharing class ActivityManagerController {

    public Agenda__c agenda{get;set;}
    public List<TopicPresenterWrapper> topicPresenterWrappers{get;set;}
    public Integer currentIndex{get;set;}
    public List<SelectOption> reviewerOptions{get;set;}
    public Map<String, Participant__c> participantsByEmailAddress;
    public Map<Id, Participant__c> participantsByIds;
    public List<Topic_Presenter__c> topicPresenters{get;set;}
    public Integer wrapperRowNumber{get;set;}
    public List<TopicPresenterWrapper> wrappersForDeletion;


    public ActivityManagerController() {
        participantsByIds = new Map<Id, Participant__c>();
        currentIndex = 0;
        String agendaId = ApexPages.currentPage().getParameters().get('id');
        if(agendaId == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Must provide an Activity to manage'));
            ApexPages.currentPage().getParameters().put('error', 'noparam');
            System.debug('No Activity Id Given');
            return;
        }
        else{
            List<Agenda__c> agendaList =  [SELECT id, Name FROM Agenda__c WHERE id = :agendaId LIMIT 1];
            if(agendaList.size() == 0){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid Activity Id'));
                ApexPages.currentPage().getParameters().put('error', 'noid');
                System.debug('Invalid Activity Id' + agendaId);
                return;
            }
            else{
                agenda = agendaList.get(0);
            }
        }

        topicPresenters = [SELECT id, Topic__c, Role__c, Presenter__c, Presenter__r.Email__c, Presenter__r.Last_Name__c, Presenter__r.First_Name__c FROM Topic_Presenter__c WHERE Agenda__c = :agendaId];

        populateReviewerOptions();

        wrappersForDeletion = new List<TopicPresenterWrapper>();

        Map<Id, Topic__c> topicsById = new Map<Id, Topic__c>();

        participantsByEmailAddress = new Map<String, Participant__c>();

        for(Topic_Presenter__c tp : topicPresenters){

            if(!topicsById.containsKey(tp.Topic__c)){
                topicsById.put(tp.Topic__c, null);
            }

            if(tp.Presenter__r.Email__c != null && !participantsByEmailAddress.containsKey(tp.Presenter__r.Email__c)){
                participantsByEmailAddress.put(tp.Presenter__r.Email__c, null);
            }
            if(!participantsByIds.containsKey(tp.Presenter__c)){
                participantsByIds.put(tp.Presenter__c, null);
            }

        }


        for(Topic__c topic : [SELECT id, Name, Projected_CME_Hours__c FROM Topic__c WHERE Id IN :topicsById.keySet()]){
            if(topicsById.get(topic.id) == null){
                topicsById.put(topic.id, topic);
            }
        }


        Map<Id, Disclosure__c> disclosuresByParticipantId = new Map<Id, Disclosure__c>();

        for(Participant__c participant : [SELECT id, Name, Salutation__c, Email__c, First_Name__c, Last_Name__c FROM Participant__c WHERE Id IN :participantsByIds.keySet()]){
            if(participantsByEmailAddress.containsKey(participant.Email__c) && participantsByEmailAddress.get(participant.Email__c) == null){
                participantsByEmailAddress.put(participant.Email__c, participant);
            }
            if(participantsByIds.get(participant.id) == null){
                participantsByIds.put(participant.id, participant);
            }
            if(!disclosuresByParticipantId.containsKey(participant.id)){
                disclosuresByParticipantId.put(participant.id, null);
            }
        }
        System.debug(participantsByEmailAddress);

        Map<Id, Disclosure__c> disclosuresById = new Map<Id, Disclosure__c>();
        for(Disclosure__c disclosure : [SELECT id, Name, Status__c, Participant__c, COI_Reviewer__c FROM Disclosure__c WHERE Participant__c IN :disclosuresByParticipantId.keySet()]){
            if(disclosuresByParticipantId.get(disclosure.Participant__c) == null){
                disclosuresByParticipantId.put(disclosure.Participant__c, disclosure);
            }
            if(!disclosuresById.containsKey(disclosure.id)){
                disclosuresById.put(disclosure.id, disclosure);
            }
        }

        Map<Id, COI__c> coisByParticipantId = new Map<Id, COI__c>();

        for(COI__c c : [SELECT id, Status__c, Disclosure__c, Disclosure__r.Participant__c FROM COI__c WHERE Disclosure__c IN :disclosuresById.keySet()]){
            if(!coisByParticipantId.containsKey(c.Disclosure__r.Participant__c)){
                coisByParticipantId.put(c.Disclosure__r.Participant__c, c);
            }
        }
        topicPresenterWrappers = new List<TopicPresenterWrapper>();
        for(Topic_Presenter__c tp : topicPresenters){
            TopicPresenterWrapper tpw = new TopicPresenterWrapper(tp, participantsByIds.get(tp.Presenter__c), disclosuresByParticipantId.get(tp.Presenter__c), topicsById.get(tp.Topic__c), coisByParticipantId.get(tp.Presenter__c));
            tpw.index = currentIndex;
            currentIndex++;
            topicPresenterWrappers.add(tpw);
        }
    }

    public void populateReviewerOptions(){
        reviewerOptions = new List<SelectOption>();
        reviewerOptions.add( new SelectOption('', '--None--'));
        for(Topic_Presenter__c tp : topicPresenters) {
            if(tp.Role__c != null && !tp.Role__c.contains(UtilConstants.Presenter)){
                SelectOption option = new SelectOption(tp.Presenter__c, tp.Presenter__r.Last_Name__c + ' - ' + tp.Role__c);
                reviewerOptions.add(option);
            }
        }
    }





    public void addTopicPresenterWrapper(){
        TopicPresenterWrapper tpw = new TopicPresenterWrapper(agenda.id);
        tpw.index = currentIndex;
        currentIndex++;
        topicPresenterWrappers.add(tpw);
    }
    public void removeTopicPresenterWrapper(){
        if(wrapperRowNumber < 0 || wrapperRowNumber >= topicPresenterWrappers.size()){
            System.debug('invalid index in removeParticipant: ' + wrapperRowNumber);
            return;
        }
        if(topicPresenterWrappers.get(wrapperRowNumber) == null ){
            System.debug('Error on removeTopicPresenter: row attempted to delete - ' + wrapperRowNumber + ', size of list - ' + topicPresenterWrappers.size());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error removing row ' + wrapperRowNumber));
            //ApexPages.currentPage().getParameters().put('error', 'rowerr');
            return;
        }
        if(topicPresenterWrappers.get(wrapperRowNumber).topicPresenter.id != null){

            wrappersForDeletion.add(topicPresenterWrappers.get(wrapperRowNumber));

        }
        if(participantsByEmailAddress.get(topicPresenterWrappers.get(wrapperRowNumber).participant.Email__c) != null){
            participantsByEmailAddress.remove(topicPresenterWrappers.get(wrapperRowNumber).participant.Email__c);
        }

        topicPresenterWrappers.remove(wrapperRowNumber);
        currentIndex--;
        resetIndexes();
    }


    public void resetIndexes(){
        for(Integer i = 0; i < topicPresenterWrappers.size(); i++){
            topicPresenterWrappers[i].index = i;
        }
    }

    public PageReference save(){

        Map<String, Topic__c> topicsByName = new Map<String, Topic__c>();
        Set<Disclosure__c> disclosureSetToUpsert = new Set<Disclosure__c>();

        System.debug(topicPresenterWrappers);
        for(TopicPresenterWrapper tpw : topicPresenterWrappers){

            if(wrapperValidation(tpw) == false){
                return null;
            }

            if(tpw.topicPresenter.Role__c == UtilConstants.Presenter || tpw.topicPresenter.Role__c == UtilConstants.PresenterUnavailable){
                if(!topicsByName.containsKey(tpw.topic.Name)){
                    topicsByName.put(tpw.topic.Name, tpw.topic);
                }
            }

            if(tpw.relatedDisclosure.Status__c == UtilConstants.PositiveDisclosure){
                disclosureSetToUpsert.add(tpw.relatedDisclosure);
            }
            if(!participantsByEmailAddress.containsKey(tpw.participant.Email__c)){
                participantsByEmailAddress.put(tpw.participant.Email__c, tpw.participant);
            }

        }

        System.debug(participantsByEmailAddress);

        for(Topic__c topic : [SELECT id, Name, Projected_CME_Hours__c FROM Topic__c WHERE Name IN :topicsByName.keyset() AND Agenda__c = :agenda.id]){

            topic.Projected_CME_Hours__c = topicsByName.get(topic.Name).Projected_CME_Hours__c;
            topicsByName.put(topic.Name, topic);

        }

        for(String key : topicsByName.keySet()){
            if(topicsByName.get(key).id == null){
                topicsByName.get(key).Agenda__c = agenda.id;
            }
        }

        Savepoint sp = Database.setSavepoint();

        try{
            upsert topicsByName.values();

            Set<Participant__c> setParticipants = new Set<Participant__c>();

            for(Participant__c participant : participantsByEmailAddress.values()){
                setParticipants.add(participant);
            }
            List<Participant__c> participantsList = new List<Participant__c>();
            participantsList.addAll(setParticipants);
            upsert participantsList;

            List<Topic_Presenter__c> topicPresentersToUpsert = new List<Topic_Presenter__c>();
            for(TopicPresenterWrapper tpw : topicPresenterWrappers){
                if(tpw.topicPresenter.Role__c != null){
                    if(tpw.topicPresenter.Role__c == UtilConstants.Presenter || tpw.topicPresenter.Role__c == UtilConstants.PresenterUnavailable){
                        tpw.topicPresenter.Topic__c = topicsByName.get(tpw.topic.Name).id;
                    }
                    if(tpw.topicPresenter.Role__c != UtilConstants.PresenterUnavailable){
                        tpw.topicPresenter.Presenter__c = participantsByEmailAddress.get(tpw.participant.Email__c).id;
                    }

                    topicPresentersToUpsert.add(tpw.topicPresenter);
                }

            }

            upsert topicPresentersToUpsert;

            List<Disclosure__c> disclosuresToUpsert = new List<Disclosure__c>();
            disclosuresToUpsert.addAll(disclosureSetToUpsert);
            upsert disclosuresToUpsert;

            System.debug(wrappersForDeletion);
            deleteWrappers();
        }
        catch(Exception e){
            Database.rollback(sp);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'There was an error processing your request.'));
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            System.debug(e.getStackTraceString());
            System.debug(e.getMessage());
            return null;
        }

        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Records Saved!'));
        return null;
    }

    public void deleteWrappers(){
        Map<Id, Participant__c> participantsToCheck = new Map<Id, Participant__c>();
        Map<Id, Topic__c> topicsToCheck = new Map<Id, Topic__c>();
        List<Topic_Presenter__c> topicPresentersToDelete = new List<Topic_Presenter__c>();
        for(TopicPresenterWrapper tpw : wrappersForDeletion) {
            topicPresentersToDelete.add(tpw.topicPresenter);

            if(tpw.participant != null && tpw.participant.id != null){
                if(!participantsToCheck.containsKey(tpw.participant.id)){
                    participantsToCheck.put(tpw.participant.id, tpw.participant);
                }
            }
            if(tpw.topic != null && tpw.topic.id != null){
                if(!topicsToCheck.containsKey(tpw.topic.id)){
                    topicsToCheck.put(tpw.topic.id, tpw.topic);
                }
            }
        }
        try{
            delete topicPresentersToDelete;

            Set<Participant__c> participantsToDelete = new Set<Participant__c>();
            for(Participant__c participant : [SELECT id , (SELECT id FROM Topic_Presenters__r) FROM Participant__c WHERE Id IN :participantsToCheck.keySet()]){
                if(participant.Topic_Presenters__r.size() == 0){
                    participantsToDelete.add(participant);
                }
            }

            Set<Topic__c> topicsToDelete = new Set<Topic__c>();
            for(Topic__c topic : [SELECT id, (SELECT id FROM Topic_Presenters__r) FROM Topic__c WHERE Id IN :topicsToCheck.keySet()]){
                if(topic.Topic_Presenters__r.size() == 0){
                    topicsToDelete.add(topic);
                }

            }
            List<Topic__c> topicList = new List<Topic__c>();
            topicList.addAll(topicsToDelete);
            List<Participant__c> participantList = new List<Participant__c>();
            participantList.addAll(participantsToDelete);
            delete topicList;
            delete participantList;

            wrappersForDeletion.clear();
        }
        catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'There was an error attempting to delete a Topic Presenter.'));
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
        }
    }
    public Boolean wrapperValidation(TopicPresenterWrapper wrapper){

        if(wrapper.participant == null || wrapper.relatedDisclosure == null || wrapper.topicPresenter == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error Saving Records'));
            System.debug(wrapper);
            return false;
        }
        if(wrapper.topicPresenter.Role__c == null || String.isBlank(wrapper.topicPresenter.Role__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Participants must have a role'));
            System.debug(wrapper);
            return false;
        }
        if(wrapper.topicPresenter.Role__c != UtilConstants.PresenterUnavailable){
            if(String.isBlank(wrapper.participant.Last_Name__c)){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Participants must have a Last Name!'));
                return false;
            }
            if(String.isBlank(wrapper.participant.Email__c)){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Participants must have an Email Address!'));
                return false;
            }
        }

        if(wrapper.topicPresenter.Role__c ==  UtilConstants.Presenter || wrapper.topicPresenter.Role__c == UtilConstants.PresenterUnavailable){
            if(String.isBlank(wrapper.topic.Name)){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Missing a Topic Title!'));
                System.debug(wrapper.topic);
                return false;
            }
        }
        return true;
    }

    public PageReference cancel(){
        if(agenda == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid Activity ID!'));
            return null;
        }
        return new PageReference('/' + agenda.id);
    }

    public class TopicPresenterWrapper{

        public Participant__c participant{get;set;}
        public Disclosure__c relatedDisclosure{get;set;}
        public Topic__c topic{get;set;}
        public Topic_Presenter__c topicPresenter{get;set;}
        public String coiStatus{get;set;}
        public Integer index{get;set;}

        public TopicPresenterWrapper(Topic_Presenter__c topicPresenter, Participant__c participant, Disclosure__c disclosure, Topic__c topic, COI__c coi){
            this.participant = participant == null ? new Participant__c() : participant;
            this.relatedDisclosure = disclosure == null ? new Disclosure__c() : disclosure;
            this.topic = topic == null ? new Topic__c() : topic;
            this.coiStatus = coi == null ? '' : coi.Status__c;
            this.topicPresenter = topicPresenter == null ? new Topic_Presenter__c() : topicPresenter;
            index = -1;
        }
        public TopicPresenterWrapper(String agendaId){
            this.participant = new Participant__c();
            this.relatedDisclosure = new Disclosure__c();
            this.topic = new Topic__c();
            this.topicPresenter = new Topic_Presenter__c();
            this.topicPresenter.Agenda__c = agendaId;
            this.coiStatus = '';
        }

    }
}